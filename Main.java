import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Введите математическую операцию: ");
        String operation = in.nextLine();
        System.out.print("Введите первое число: ");
        double number1 = in.nextInt();
        System.out.print("Введите второе число: ");
        double number2 = in.nextInt();

        double result = 0;
        String error = " ";
        switch (operation) {
            case "/":
                result = number1 / number2;
                System.out.println("Ответ: " + result);
                break;
            case "*":
                result = number1 * number2;
                System.out.println("Ответ: " + result);
                break;
            case "+":
                result = number1 + number2;
                System.out.println("Ответ: " + result);
                break;
            case "-":
                result = number1 - number2;
                System.out.println("Ответ: " + result);
                break;
            default:
                error = "Неверный символ";
                System.out.println(error);
                break;
        }
    }
}